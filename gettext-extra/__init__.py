# system modules
import shutil

# internal modules

# external modules
from SCons.Variables import Variables, PathVariable
from SCons.Builder import Builder
from SCons.Script import ARGUMENTS


def generate(env):
    variables = Variables(None, ARGUMENTS)
    variables.Add(
        PathVariable(
            "msgcat",
            "msgcat executable path",
            env.WhereIs("msgcat") or "msgcat",
            # don't force this path to exists when displaying --help page
            PathVariable.PathAccept
            if env.GetOption("help")
            else PathVariable.PathExists,
        )
    )

    # Add variables to environment
    variables.Update(env)

    # Add variables to help
    env.Help(variables.GenerateHelpText(env), append=True)

    env.SetDefault(
        MSGCAT=env["msgcat"],
        MSGCATCOM="$MSGCAT $MSGCATFLAGS -o $TARGET $SOURCES",
    )

    # create a svg2png builder using Inkscape
    pot_concatenator_builder = Builder(
        action="$MSGCATCOM", src_suffix=env.get("POTSUFFIX", ".pot"),
    )

    # Add pot concatenator builder to environment
    env.Append(BUILDERS={"CombinePOTs": pot_concatenator_builder})


def exists(env):
    # Doesn't really make sense to do anything here, see
    # https://github.com/SCons/scons/issues/3005
    return True
