# system modules
import os
import operator
import itertools

# internal modules

# external modules
from SCons.Script.Main import AddOption, GetOption

__doc__ = """
This tool adds rudimentary autotools-like funcitonality to SCons construction
environments:

- ``--package-name`` to set the package name (defaults to the value of the
  ``PACKAGE_NAME`` construction variable or to the current directory's name
  otherwise)
- typical options (like ``--prefix``, ``--sysconfdir``, etc.)
- All options are added in uppercase and underscored instead of dashes as
  construction variables
"""


def generate(env):
    AddOption(
        "--package-name",
        dest="package_name",
        type="string",
        nargs=1,
        action="store",
        default=env.get("PACKAGE_NAME", os.path.basename(os.getcwd())),
        metavar="NAME",
        help="name of the package",
    )

    options = (
        ("--prefix", "prefix", "installation prefix", "/usr/local"),
        ("--exec-prefix", "exec_prefix", "installation prefix", "$PREFIX"),
        ("--bindir", "bindir", "user executables [EPREFIX/bin]", "$EXEC_PREFIX/bin",),
        (
            "--sbindir",
            "sbindir",
            "system admin executables [EPREFIX/sbin]",
            "$EXEC_PREFIX/sbin",
        ),
        (
            "--libexecdir",
            "libexecdir",
            "program executables [EPREFIX/libexec]",
            "$EXEC_PREFIX/libexec",
        ),
        (
            "--sysconfdir",
            "sysconfdir",
            "read-only single-machine data [PREFIX/etc]",
            "$PREFIX/etc",
        ),
        (
            "--sharedstatedir",
            "sharedstatedir",
            "modifiable architecture-independent data [PREFIX/com]",
            "$PREFIX/com",
        ),
        (
            "--localstatedir",
            "localstatedir",
            "modifiable single-machine data [PREFIX/var]",
            "$PREFIX/var",
        ),
        (
            "--runstatedir",
            "runstatedir",
            "modifiable per-process data [LOCALSTATEDIR/run]",
            "$LOCALSTATEDIR/run",
        ),
        (
            "--libdir",
            "libdir",
            "object code libraries [EPREFIX/lib]",
            "$EXEC_PREFIX/lib",
        ),
        (
            "--includedir",
            "includedir",
            "C header files [PREFIX/include]",
            "$PREFIX/include",
        ),
        (
            "--datarootdir",
            "datarootdir",
            "read-only arch.-independent data root [PREFIX/share]",
            "$PREFIX/share",
        ),
        (
            "--datadir",
            "datadir",
            "read-only architecture-independent data [DATAROOTDIR]",
            "$DATAROOTDIR",
        ),
        (
            "--infodir",
            "infodir",
            "info documentation [DATAROOTDIR/info]",
            "$DATAROOTDIR/info",
        ),
        (
            "--localedir",
            "localedir",
            "locale-dependent data [DATAROOTDIR/locale]",
            "$DATAROOTDIR/locale",
        ),
        (
            "--mandir",
            "mandir",
            "man documentation [DATAROOTDIR/man]",
            "$DATAROOTDIR/man",
        ),
        (
            "--docdir",
            "docdir",
            "documentation root [DATAROOTDIR/doc/PACKAGE_NAME]",
            "$DATAROOTDIR/doc/$PACKAGE_NAME",
        ),
        ("--htmldir", "htmldir", "html documentation [DOCDIR]", "$DOCDIR"),
        ("--dvidir", "dvidir", "dvi documentation [DOCDIR]", "$DOCDIR"),
        ("--pdfdir", "pdfdir", "pdf documentation [DOCDIR]", "$DOCDIR"),
        ("--psdir", "psdir", "ps documentation [DOCDIR]", "$DOCDIR"),
    )

    for option, dest, helpstr, default in options:
        AddOption(
            option,
            dest=dest,
            type="string",
            nargs=1,
            action="store",
            metavar="DIR",
            help=helpstr,
            default=default,
        )

    # Initialize an empty environment from the command-line options
    env.Replace(
        **{
            option.upper(): GetOption(option)
            for option in itertools.chain(
                map(operator.itemgetter(1), options), ("package_name",)
            )
        }
    )


def exists():
    return True
