# system modules
import os

# internal modules

# external modules
from SCons.Script.Main import AddOption, GetOption

__doc__ = """
This tool adds the variables of the XDG base directory specification to the
construction environment

https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
"""


def generate(env):
    for varname, subdir in dict(
        XDG_CONFIG_HOME=".config",
        XDG_CACHE_HOME=".cache",
        XDG_DATA_HOME=os.path.join(".local", "share"),
        XDG_STATE_HOME=os.path.join(".local", "state"),
    ).items():
        env[varname] = os.path.join(
            os.environ.get(varname, os.path.join(os.path.expanduser("~"), subdir))
        )
    for varname, default in dict(
        XDG_DATA_DIRS=":".join(
            [
                os.path.join(os.path.sep, "usr", "local", "share"),
                os.path.join(os.path.sep, "usr", "share"),
            ]
        ),
        XDG_CONFNIG_DIRS=os.path.join(os.path.sep, "etc", "xdg"),
    ).items():
        env[varname] = os.environ.get(varname, default)


def exists():
    return True
