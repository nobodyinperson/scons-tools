# system modules
import shlex
import os
import re
import itertools
import json

# internal modules

# external modules
from SCons.Builder import Builder
from SCons.Script import Exit

# https://gitlab.com/tue-umphy/software/parmesan/-/blob/5b97cda7e65fab5aaf00066217ccd3370bcf4201/parmesan/utils/__init__.py#L42
def is_iterable(x):
    """
    Check if a given object is iterable but not a string.
    """
    if isinstance(x, str):
        return False
    try:
        iter(x)
    except TypeError:
        return False
    return True


def sanitize(s, forbidden=r"[^a-zA-Z0-9_=+-]+", sep="-"):
    return re.sub(f"{re.escape(str(sep))}+", str(sep), re.sub(forbidden, str(sep), s))


# taken from https://gitlab.com/tue-umphy/software/parmesan/-/blob/5b97cda7e65fab5aaf00066217ccd3370bcf4201/parmesan/utils/__init__.py#L83
def all_argument_combinations(d):
    """
    Given a dict mapping argument names to (sequences of) possible values,
    return an iterable of all argument combinations yielding :any:`dict` s of
    the same size as the input but only one value.

    For example:

    .. code-block:: python

        all_argument_combinations({"a":[1,2,3],"b":[4,5,6]})
        # yields sequentially
        {'a': 1, 'b': 4}
        {'a': 1, 'b': 5}
        {'a': 1, 'b': 6}
        {'a': 2, 'b': 4}
        {'a': 2, 'b': 5}
        {'a': 2, 'b': 6}
        {'a': 3, 'b': 4}
        {'a': 3, 'b': 5}
        {'a': 3, 'b': 6}
    """
    return map(
        dict,
        itertools.product(
            *(
                (
                    itertools.product(
                        (arg,), values if is_iterable(values) else (values,)
                    )
                )
                for arg, values in d.items()
            )
        ),
    )


def STLCombinations(env, sources, parameters=None, **kwargs):
    def stl(source):
        for params in all_argument_combinations(parameters):
            subenv = env.Clone()
            subenv.MergeFlags(dict(OPENSCAD_FLAGS=kwargs.get("OPENSCAD_FLAGS", "")))
            subenv.MergeFlags(
                dict(
                    OPENSCAD_FLAGS=[
                        f"-D{shlex.quote(str(k))}={shlex.quote(str(v))}"
                        for k, v in sorted(params.items())
                    ]
                )
            )

            def sanitize(s, sep="_"):
                sepe = re.escape(sep)
                return re.sub(
                    f"(^{sepe}+)|({sepe}+$)",
                    "",
                    re.sub(f"{sepe}+", sep, re.sub(r"[^a-zA-Z0-9_.,-]+", sep, s)),
                )

            target = (
                "_".join(
                    itertools.chain(
                        (os.path.splitext(str(source))[0],),
                        (
                            f"{sanitize(str(k))}={sanitize(str(v))}"
                            for k, v in sorted(params.items())
                        ),
                    )
                )
                + ".stl"
            )
            yield subenv.STL(target, source)

    return [list(stl(s)) for s in env.Flatten(sources)]


def openscad_variables_to_cli_options(variables):
    return " ".join(
        f"-D{shlex.quote(str(k))}={shlex.quote(str(v))}"
        for k, v in getattr(variables, "items", tuple)()
    )


def openscad_variable_name_shrink_camelcase_short(s, env=None):
    return "".join(
        x.title()
        for x in (
            re.sub(r"^([^aeiou]*[aeiou]|).*$", r"\1", t) for t in re.split(r"_", s)
        )
    )


def openscad_variable_name_shrink_camelcase_full(s, env=None):
    return "".join(x.title() for x in re.split(r"_", s))


def openscad_variable_name_shrink(s, env=None):
    return "_".join(
        re.sub(r"^([^aeiou]*[aeiou]|).*$", r"\1", t) for t in re.split(r"_", s)
    )


def openscad_variables_for_filename(
    variables, shrinker=lambda s, env=None: s, env=None
):
    whitelist = set(
        (env or dict()).get("OPENSCAD_VARIABLES_INCLUDE_IN_FILENAME", set())
    )
    blacklist = set(
        (env or dict()).get("OPENSCAD_VARIABLES_EXCLUDE_IN_FILENAME", set())
    )
    return "-".join(
        f"{shrinker(sanitize(k))}={sanitize(str(v))}"
        for k, v in getattr(variables, "items", tuple)()
        if (k not in blacklist and ((whitelist and k in whitelist) or not whitelist))
    )


def splitext(filename, old_suffix=None):
    if m := re.fullmatch(
        r"(.*?)"
        + (f"({re.escape(str(old_suffix))})" if old_suffix else r"(\.[a-z]+)+$"),
        str(filename),
        flags=re.IGNORECASE,
    ):
        return m.groups()
    else:
        return (filename, "")


def openscad_get_parameter_file(scadfile):
    return f"{splitext(str(scadfile))[0]}.json"


def openscad_get_parameter_sets(scadfile):
    try:
        with open(openscad_get_parameter_file(str(scadfile))) as fh:
            d = json.load(fh)
            return tuple(d.get("parameterSets", {}).keys())
    except FileNotFoundError:
        return tuple()


def make_emitter(target_suffix, builder="?", src_suffix=None):
    target_suffix = re.sub(r"^\.*", ".", target_suffix)

    def emitter(source, target, env):
        if len(source) != 1:
            print(
                f"Builder {builder} needs exactly 1 source, not {len(source)}): {source}"
            )
            Exit(1)
        if len(target) != 1:
            print(
                f"Builder {builder} needs exactly 1 target, not {len(target)}): {target}"
            )
            Exit(1)
        s, t = source[0], target[0]
        name, ext = splitext(str(t))
        if parameter_set := env.subst("$OPENSCAD_PARAMETER_SET"):
            if not (parameter_file := env.subst("$OPENSCAD_PARAMETER_FILE")):
                parameter_file = f"{name}.json"
                # print(f"Assuming the parameter file for {str(s)!r} is {str(parameter_file)!r}"
            source += [parameter_file]
        custom_target = os.path.abspath(
            f"{splitext(s)[0]}{target_suffix}"
        ) != os.path.abspath(str(t))
        if not custom_target:
            target = "".join(
                (
                    "-".join(
                        [name]
                        + [
                            s
                            for s in (
                                env.subst("$GIT_COMMIT_DESCRIPTION"),
                                openscad_variables_for_filename(
                                    env["OPENSCAD_VARIABLES"],
                                    shrinker=env["OPENSCAD_VARIABLE_NAME_SHRINKER"],
                                    env=env,
                                ),
                                sanitize(env.subst("$OPENSCAD_PARAMETER_FILE")),
                                sanitize(env.subst("$OPENSCAD_PARAMETER_SET")),
                                env.subst("$OPENSCAD_FLAGS"),
                            )
                            if s
                        ]
                    ),
                    target_suffix,
                )
            )
        return target, source

    return emitter


def generate(env):
    # so we can quote names from within actions
    env["shlex"] = shlex
    conf = env.Configure()
    if openscad := conf.CheckProg("openscad"):
        env["OPENSCAD"] = openscad
    else:
        print(f"Can't find OpenSCAD")
        Exit(2)
    headless = not any(map(os.environ.get, ("DISPLAY", "WAYLAND_DISPLAY")))
    if xvfb_run := conf.CheckProg("xvfb-run"):
        env["XVFB_RUN"] = xvfb_run
    else:
        if headless:
            print(
                "Warning: Running headlessly but xvfb-run is not installed. "
                "OpenSCAD will probably throw an error during rendering. "
                "Consider installing xvfb-run, then OpenSCAD will be run inside it."
            )
    conf.Finish()
    # Add construction variables
    env["sanitize"] = sanitize
    env["splitext"] = splitext
    env["openscad_variables_to_cli_options"] = openscad_variables_to_cli_options
    env["openscad_variables_for_filename"] = openscad_variables_for_filename
    env["openscad_get_parameter_file"] = openscad_get_parameter_file
    env["openscad_get_parameter_sets"] = openscad_get_parameter_sets
    env[
        "openscad_variable_name_shrink_camelcase_full"
    ] = openscad_variable_name_shrink_camelcase_full
    env[
        "openscad_variable_name_shrink_camelcase_short"
    ] = openscad_variable_name_shrink_camelcase_short
    env["openscad_variable_name_shrink"] = openscad_variable_name_shrink
    env["OPENSCAD_VARIABLES"] = ""
    env["OPENSCAD_VARIABLE_NAME_SHRINKER"] = lambda s, env=None: s
    env["OPENSCAD_PARAMETER_FILE"] = ""
    env["OPENSCAD_PARAMETER_SET"] = ""
    # Add builders
    env.Append(
        BUILDERS={
            "Render": Builder(
                action="${('$XVFB_RUN $XVFB_RUN_FLAGS -n' + str(next(XVFB_RUN_SERVER_NR))) if (USE_XVFB and XVFB_RUN) else ''} "
                "$OPENSCAD "
                "${('-p '+shlex.quote(str(OPENSCAD_PARAMETER_FILE))) if OPENSCAD_PARAMETER_FILE else ''} "
                "${('-P '+shlex.quote(str(OPENSCAD_PARAMETER_SET))) if OPENSCAD_PARAMETER_SET else ''} "
                "${openscad_variables_to_cli_options(OPENSCAD_VARIABLES)} "
                "$OPENSCAD_FLAGS "
                "-o ${shlex.quote(str(TARGET))} "
                "${shlex.quote(str(SOURCE))}",
                ENV={
                    k: v
                    for k, v in os.environ.items()
                    if k in ("DISPLAY", "XAUTHORITY", "WAYLAND_DISPLAY")
                },
                USE_XVFB=headless,
                XVFB_RUN_FLAGS="-a",
                XVFB_RUN_SERVER_NR=itertools.count(100),
                suffix=".png",
                emitter=make_emitter(".png", builder="Render"),
            ),
            "STL": Builder(
                action="$OPENSCAD "
                "${('-p '+shlex.quote(str(OPENSCAD_PARAMETER_FILE))) if OPENSCAD_PARAMETER_FILE else ''} "
                "${('-P '+shlex.quote(str(OPENSCAD_PARAMETER_SET))) if OPENSCAD_PARAMETER_SET else ''} "
                "${openscad_variables_to_cli_options(OPENSCAD_VARIABLES)} "
                "$OPENSCAD_FLAGS "
                "-o ${shlex.quote(str(TARGET))} "
                "${shlex.quote(str(SOURCE))}",
                suffix=".stl",
                emitter=make_emitter(".stl", builder="STL"),
            ),
        }
    )
    env.AddMethod(STLCombinations)


def exists(env):
    # Doesn't really make sense to do anything here, see
    # https://github.com/SCons/scons/issues/3005
    return True
