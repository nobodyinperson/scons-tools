# system modules
import subprocess
import warnings
import re

# internal modules

# external modules
from SCons.Variables import Variables, PathVariable
from SCons.Script import ARGUMENTS


def generate(env):
    variables = Variables(None, ARGUMENTS)
    variables.Add(
        PathVariable(
            "git",
            "git executable path",
            env.WhereIs("git") or "git",
            # don't force this path to exists when displaying --help page
            PathVariable.PathAccept
            if env.GetOption("help")
            else PathVariable.PathExists,
        )
    )

    # Add variables to environment
    variables.Update(env)

    # Add variables to help
    env.Help(variables.GenerateHelpText(env), append=True)

    # Determine git version description
    try:
        version = subprocess.check_output(
            [
                env.subst("$git"),
                "describe",
                "--tags",
                "--always",
                "--dirty",
                "--match",
                "v*",
            ]
        )
        env.Append(
            GIT_VERSION_DESCRIPTION=re.sub(
                r"^\D+", r"", version.decode(errors="ignore").strip()
            )
        )
        commit = subprocess.check_output(
            [
                env.subst("$git"),
                "describe",
                "--always",
                "--dirty",
            ]
        )
        env.Append(
            GIT_COMMIT_DESCRIPTION=re.sub(
                r"^[^a-f0-9]+", r"", commit.decode(errors="ignore").strip()
            )
        )
    except (FileNotFoundError, subprocess.CalledProcessError) as e:
        warnings.warn("Couldn't determine version description from git: {}".format(e))


def exists(env):
    # Doesn't really make sense to do anything here, see
    # https://github.com/SCons/scons/issues/3005
    return True
