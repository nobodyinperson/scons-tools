# system modules
import shlex
import subprocess
import warnings
import re

# internal modules

# external modules
from SCons.Variables import Variables, PathVariable
from SCons.Builder import Builder
from SCons.Script import ARGUMENTS


def generate(env):
    variables = Variables(None, ARGUMENTS)
    variables.Add(
        PathVariable(
            "pandoc",
            "pandoc executable path",
            env.WhereIs("pandoc") or "pandoc",
            # don't force this path to exists when displaying --help page
            PathVariable.PathAccept
            if env.GetOption("help")
            else PathVariable.PathExists,
        )
    )

    # Add variables to environment
    variables.Update(env)

    # Add variables to help
    env.Help(variables.GenerateHelpText(env), append=True)

    version_output = subprocess.check_output(
        [env.get("pandoc"), "--version"], stderr=subprocess.STDOUT
    ).decode(errors="ignore")
    m = re.search(
        string=version_output,
        pattern=r"pandoc\s+(\d+)(?:\.(\d+)(?:\.(\d+))?)?",
        flags=re.IGNORECASE,
    )

    def to_int(s):
        try:
            return int(s)
        except BaseException:
            return int(0)

    if m:
        env["PANDOC_VERSION"] = tuple(map(to_int, m.groups()))
    else:
        warnings.warn("Couldn't determine pandoc version!")

    def pandoc_builder_emitter(target, source, env):
        if template := env.subst("$PANDOC_TEMPLATE"):
            env.Depends(target, template)
        return target, source

    pandoc_builder = Builder(
        action="$pandoc $PANDOC_FLAGS "
        "${('--template '+shlex.quote(str(PANDOC_TEMPLATE))) if PANDOC_TEMPLATE else ''} "
        "-o $TARGET $SOURCES",
        shlex=shlex,
        emitter=pandoc_builder_emitter,
    )

    env.Append(BUILDERS={"PandocConvert": pandoc_builder})


def exists(env):
    # Doesn't really make sense to do anything here, see
    # https://github.com/SCons/scons/issues/3005
    return True
