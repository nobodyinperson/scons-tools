# system modules
import os

# internal modules

# external modules
from SCons.Builder import Builder
from SCons.Action import Action
from SCons.Script import Exit


def symlink_action(target, source, env):
    if not (len(env.Flatten(source)) == 1 and len(env.Flatten(target)) == 1):
        print(f"Can only create a symlink from a single file to a single destination")
        Exit(2)
    source = next(map(str, iter(env.Flatten(source))))
    target = next(map(str, iter(env.Flatten(target))))
    if os.path.realpath(source) != os.path.realpath(target):
        os.symlink(
            os.path.relpath(source, os.path.dirname(target))
            if env.get("SYMLINK_RELATIVE", False)
            else os.path.abspath(source),
            target,
            target_is_directory=env.get("SYMLINK_TARGET_IS_DIRECTORY", False),
        )


def hardlink_action(target, source, env):
    if not (len(env.Flatten(source)) == 1 and len(env.Flatten(target)) == 1):
        print(f"Can only create a symlink from a single file to a single destination")
        Exit(2)
    source = next(map(str, iter(env.Flatten(source))))
    target = next(map(str, iter(env.Flatten(target))))
    os.link(
        os.path.abspath(source),
        os.path.abspath(target),
        follow_symlinks=env.get("HARDLINK_FOLLOW_SYMLINKS", True),
    )


def Symlink(target, source):
    return Action(
        symlink_action,
        cmdstr=f"Create symlink {target} pointing to {source}",
    )


def Hardlink(target, source):
    return Action(
        hardlink_action,
        cmdstr=f"Create hardlink {target} pointing to {source}",
    )


def generate(env):
    # Add builders
    env.Append(
        BUILDERS={
            "Symlink": Builder(
                action=Symlink("$TARGET", "$SOURCE"),
                SYMLINK_TARGET_IS_DIRECTORY=False,
            ),
            "Hardlink": Builder(
                action=Hardlink("$TARGET", "$SOURCE"),
                HARDLINK_FOLLOW_SYMLINKS=True,
            ),
        },
        Symlink=Symlink,
        Hardlink=Hardlink,
    )


def exists(env):
    # Doesn't really make sense to do anything here, see
    # https://github.com/SCons/scons/issues/3005
    return True
