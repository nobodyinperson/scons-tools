# system modules
import shutil
import subprocess
import warnings
import re
import os

# internal modules

# external modules
from SCons.Variables import Variables, PathVariable
from SCons.Builder import Builder
from SCons.Script import ARGUMENTS


def generate(env):
    variables = Variables(None, ARGUMENTS)
    variables.Add(
        PathVariable(
            "inkscape",
            "Inkscape executable path",
            env.WhereIs("inkscape") or "inkscape",
            # don't force this path to exists when displaying --help page
            PathVariable.PathAccept
            if env.GetOption("help")
            else PathVariable.PathExists,
        )
    )

    # Add variables to environment
    variables.Update(env)

    # Add variables to help
    env.Help(variables.GenerateHelpText(env), append=True)

    inkscape_version_output = subprocess.check_output(
        [env.get("inkscape"), "--version"], stderr=subprocess.STDOUT
    ).decode(errors="ignore")
    match = re.search(
        string=inkscape_version_output,
        pattern=r"inkscape\s+(\d+)(?:\.(\d+)(?:\.(\d+))?)?",
        flags=re.IGNORECASE,
    )

    def to_int(s):
        try:
            return int(s)
        except BaseException:
            return int(0)

    if match:
        env["INKSCAPE_VERSION"] = tuple(map(to_int, match.groups()))
    else:
        warnings.warn("Couldn't determine Inkscape version!")

    def append_width_x_height(env, sources):

        try:
            d = {
                k: int(v)
                for k, v in env.Dictionary().items()
                if k in ("WIDTH", "HEIGHT")
            }
            assert all(x > 0 for x in d.values())
            return "-{WIDTH}x{HEIGHT}.png".format(**d)
        except (KeyError, TypeError, ValueError, AssertionError):
            raise SCons.Errors.UserError(
                "Both WIDTH ({WIDTH}) and HEIGHT ({HEIGHT}) must "
                "be set to positive integers in the construction environment "
                "for the svg2png Builder".format(
                    **{k: env.get(k, "-unset-") for k in ("WIDTH", "HEIGHT")}
                )
            )

    inkscape_svg2png_cmds = {
        # as of Inkscape v1, the cli interface changed
        (1, 0, 0): (
            "$inkscape --export-filename='$TARGET' -w $WIDTH -h $HEIGHT" " '$SOURCE'"
        ),
        # for pre-v1-versions this is the cli command
        (0, 0, 0): (
            "$inkscape --without-gui "
            "--export-png '$TARGET' "
            "-w=$WIDTH -h=$HEIGHT '$SOURCE'"
        ),
    }

    inkscape_svg2png_cmd = next(
        (
            vc[1]
            for vc in sorted(inkscape_svg2png_cmds.items(), reverse=True)
            if env.get("INKSCAPE_VERSION", (0, 0, 0)) >= vc[0]
        ),
        None,
    )

    if not inkscape_svg2png_cmd:
        warnings.warn(
            "Don't know how to convert SVG to PNG with Inkscape version {}".format(
                env.get("INKSCAPE_VERSION", "?")
            )
        )
        return

    # create a svg2png builder using Inkscape
    svg2png_builder = Builder(
        action=inkscape_svg2png_cmd,
        suffix=append_width_x_height,
        src_suffix=[".svg"],
    )

    def make_inkscape_builder(filetype):
        return Builder(
            action="inkscape $INKSCAPE_FLAGS --export-type=$INKSCAPE_EXPORT_TYPE -o $TARGET $SOURCE",
            INKSCAPE_EXPORT_TYPE=str(filetype).lower(),
            suffix=f".{str(filetype).lower()}",
            src_suffix=".svg",
        )

    env.Append(
        BUILDERS={
            f"Inkscape{str(fmt).upper()}": make_inkscape_builder(fmt)
            for fmt in ("svg", "png", "ps", "eps", "pdf", "emf", "wmf")
        }
    )

    # Add svg2png builder to environment
    env.Append(BUILDERS={"PNG": svg2png_builder})


def exists(env):
    # Doesn't really make sense to do anything here, see
    # https://github.com/SCons/scons/issues/3005
    return True
