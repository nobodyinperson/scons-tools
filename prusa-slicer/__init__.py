# system modules
import shlex
import difflib
import os
import re

# internal modules

# external modules
from SCons.Builder import Builder
from SCons.Script import Exit, GetOption, Glob, Move


def AddPrusaProfileSyncAlias(
    env,
    profile_dir_local,
    profile_dir_prusa=os.environ.get(
        "XDG_CONFIG_HOME",
        os.path.join(os.path.expanduser("~"), ".config", "PrusaSlicer"),
    ),
    similar_names=None,
    alias="sync-prusa-profiles",
):
    env.Alias(alias)

    profile_dir_local = "prusa-profiles"
    repo_prusa_profiles = Glob(os.path.join(profile_dir_local, "*", "*.ini"))

    profile_dir_prusa = os.path.join(env["XDG_CONFIG_HOME"], "PrusaSlicer")
    prusa_profiles = Glob(os.path.join(profile_dir_prusa, "*", "*.ini"))

    def filename_similarity(filename1, filename2):
        """
        Determines how ”similar” an OpenScad filename and a Prusa profile name look
        like.
        """

        def normalize_filename(s):
            return " ".join(  # join words together by space
                re.split(  # split words
                    r"\W+",
                    re.sub(
                        r"(\.\w+)+$", r"", os.path.basename(str(s))
                    ),  # remove suffix(es)
                )
            ).lower()  # lower case

        changes1 = tuple(
            difflib.ndiff(*map(normalize_filename, (filename1, filename2)))
        )
        changes2 = tuple(
            difflib.ndiff(*map(normalize_filename, (filename2, filename1)))
        )

        def score(changes):
            additions = sum(s.startswith("+") for s in changes)
            removals = sum(s.startswith("-") for s in changes)
            similarities = sum(s.startswith(" ") for s in changes)
            return max(0, (similarities - removals)) / similarities

        return max(map(score, (changes1, changes2)))

    # copy relevant prusa configs into our repo
    for prusa_profile in prusa_profiles:
        if not os.path.exists(str(prusa_profile)):
            # Glob() also finds potentially newly created targets, so our new
            # symlinks as well. But those we want to ignore.
            continue
        if matching_repo_profile := next(
            (
                p
                for p in repo_prusa_profiles
                if os.path.realpath(str(p)) == os.path.realpath(str(prusa_profile))
            ),
            None,
        ):
            if "explain" in GetOption("debug"):
                print(
                    f"{prusa_profile!s} is already a link to {matching_repo_profile!s}, skipping!"
                )
            continue
        for similar_name in similar_names or []:
            if (similarity := filename_similarity(similar_name, prusa_profile)) > 0.8:
                if "explain" in GetOption("debug"):
                    print(
                        f"similarity of {similar_name!s} and {prusa_profile!s} is {similarity}: Copy it!"
                    )
                prusa_profile_copy_in_repo = env.Command(
                    target=os.path.join(
                        profile_dir_local,
                        os.path.relpath(
                            str(prusa_profile), start=str(profile_dir_prusa)
                        ),
                    ),
                    source=prusa_profile,
                    action=[
                        Move("$TARGET", "$SOURCE"),
                        env["Symlink"]("$SOURCE", "$TARGET"),
                    ],
                )
                env.Depends(alias, prusa_profile_copy_in_repo)

    # Link all of our repo configs into the Prusa config dir
    for configfile in repo_prusa_profiles:
        link = env.Symlink(
            os.path.join(
                profile_dir_prusa,
                os.path.relpath(str(configfile), start=profile_dir_local),
            ),
            configfile,
        )
        env.Depends(alias, link)


def generate(env):
    # so we can quote names from within actions
    env["shlex"] = shlex
    conf = env.Configure()
    if prusa_slicer := conf.CheckProg("prusa-slicer"):
        env["PRUSA_SLICER"] = prusa_slicer
    else:
        print(f"Can't find PrusaSlicer")
        Exit(2)
    conf.Finish()
    # Add builders
    env.Append(
        BUILDERS={
            "GCode": Builder(
                action="$PRUSA_SLICER $PRUSA_SLICER_FLAGS --output ${shlex.quote(str(TARGET))} ${' '.join(map(shlex.quote,map(str,SOURCES)))}",
                PRUSA_SLICER_FLAGS="--export-gcode --merge",
                suffix=".gcode",
                src_suffix=".stl",
            ),
        }
    )
    env.AddMethod(AddPrusaProfileSyncAlias)


def exists(env):
    # Doesn't really make sense to do anything here, see
    # https://github.com/SCons/scons/issues/3005
    return True
